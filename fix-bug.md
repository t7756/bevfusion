注释：File "/usr/src/bevfusion/mmdet3d/ops/feature_decorator/feature_decorator.py", line 4, in <module>
中的下面这行
#  from . import feature_decorator_ext

注释：File "/usr/src/bevfusion-main/bevfusion/mmdet3d/ops/feature_decorator/__init__.py", line 1, in <module>
中的下面这行
# from .feature_decorator import feature_decorator




    if test:
        print('test sample: {}'.format(len(train_nusc_infos)))
        data = dict(infos=train_nusc_infos, metadata=metadata)
        #info_path = osp.join(root_path,
        #                     '{}_infos_test_radar.pkl'.format(info_prefix))
        info_path = osp.join(root_path,
                             '{}_infos_test.pkl'.format(info_prefix))
        mmcv.dump(data, info_path)
    else:
        print(info_prefix)
        print('train sample: {}, val sample: {}'.format(
            len(train_nusc_infos), len(val_nusc_infos)))
        data = dict(infos=train_nusc_infos, metadata=metadata)
        #info_path = osp.join(info_prefix,
        #                     '{}_infos_train_radar.pkl'.format(info_prefix))
        info_path = osp.join(root_path,
                             '{}_infos_train.pkl'.format(info_prefix))
        mmcv.dump(data, info_path)
        data['infos'] = val_nusc_infos
        #info_val_path = osp.join(info_prefix,
        #                         '{}_infos_val_radar.pkl'.format(info_prefix))
        info_val_path = osp.join(root_path,
                             '{}_infos_val.pkl'.format(info_prefix))
        mmcv.dump(data, info_val_path)


download_pretrained.sh
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/bevfusion-det.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/bevfusion-seg.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained/lidar-only-det.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained/lidar-only-seg.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/camera-only-det.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/camera-only-seg.pth &&
wget https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/swint-nuimages-pretrained.pth

grep -r "https://github.com/SwinTransformer/storage/releases/download/v1.0.0/swin_tiny_patch4_window7_224.pth" .

#打开：./configs/nuscenes/det/centerhead/lssfpn/camera/256x704/swint/default.yaml:

# 更改checkpoint ：https://hanlab18.mit.edu/projects/bevfusion/files/pretrained_updated/swint-nuimages-pretrained.pth


# mmdet3d/models/vtransforms/base:第 38 行：
# add_depth_features=True  改为 False 